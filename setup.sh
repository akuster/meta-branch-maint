#!/bin/bash
#set -x

CI_PROJECT_DIR=$1
ARCHIVE_DIR=$2
BRANCH=$3

cd $CI_PROJECT_DIR/poky 

BUILD_DIR=$CI_PROJECT_DIR/build

cp $CI_PROJECT_DIR/conf/auto.conf $BUILD_DIR/conf/.
cp $CI_PROJECT_DIR/conf/exclude_world.inc $BUILD_DIR/conf/.
cp $CI_PROJECT_DIR/conf/site.conf $BUILD_DIR/conf/.

sed -i -e "s|##ARCHIVE_DIR##|${ARCHIVE_DIR}|g" $BUILD_DIR/conf/site.conf
sed -i -e "s|##BRANCH##|${BRANCH}|g" $BUILD_DIR/conf/site.conf
